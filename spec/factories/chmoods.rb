# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :chmood do
    user_id 1
    colour_id 1
    type_id 1
    content "MyString"
    desc "MyString"
    tags "MyString"
    created_at "2014-03-28 11:24:40"
  end
end
