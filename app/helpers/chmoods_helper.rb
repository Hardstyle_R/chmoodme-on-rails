module ChmoodsHelper


  # adds 3 dots if the text is too long
  def dottize(s, after_length)

    dots = (s.length > after_length) ? ' ...' : ''

    if (s.split(' ').count > 1)
      s.split(" ").each_with_object("") {|x,ob| break ob unless (ob.length + " ".length + x.length <= after_length);ob << (" " + x)}.strip
      s + dots
    else
      s[0..(after_length-1)] + dots
    end
  end


end
