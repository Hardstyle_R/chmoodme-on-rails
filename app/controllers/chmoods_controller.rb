class ChmoodsController < ApplicationController

  before_filter :authenticate_user!

  def add

    if request.post? && !params.blank?


      #chmood = Chmood.new(data)

      # @chmood = current_user.chmoods.build(user_params)
      @chmood = Chmood.new(user_params)
      @chmood.user_id = current_user.id
      @chmood.created_at = Time.now
      @chmood.updated_at = Time.now

      if @chmood.save
        flash[:notice] = "Woohoo! Your chmood has been created!"
        redirect_to :back # chmoods_add_path
      else
        flash[:error] = "Smth gone wrong...\r\n" # + @chmood.errors.to_a.join("\r\n")
      end

    else
      @user = current_user
      @chmood = Chmood.new(:user_id => @user.id, :colour_id => rand(1..5), :type_id => 1)
    end

  end

  def show

  end


  def comment_add

    @chmood_comment = Comment.build_from(Chmood.find(user_params[:chmood_id]), current_user.id, user_params[:body]);

    respond_to do |format|
      if @chmood_comment.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.js   {}
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end


    #if request.xhr?
    #  render :json => @chmood_comment
    #end
  end


  private

  def user_params
    params.require(:chmood).permit(:colour_id, :type_id, :content, :desc, :tags, :image)
  end

  def user_params_comment
    params.require(:comment).permit(:chmood_id, :body, :user_id)
  end

end
