class HomeController < ApplicationController

  before_filter :authenticate_user!

  MAX_COLOURS = 5

  PAGE_TITLES = {1 => '[Comedy] - have Fun', 2 => 'Be [Inspired]', 3 => '[Discover] new', 4 => '[Create] smth special', 5 => 'Keep [Moments]'}

  def index


    if params[:colour_id].present?

      @colour_id = params[:colour_id].to_i

    else
      @colour_id = 1 # hardcoded
    end

    @next = @colour_id + 1

    if @next > MAX_COLOURS
      @next = 1
    end

    @prev = @colour_id - 1

    if @prev <= 0
      @prev = MAX_COLOURS
    end


    @page_title = PAGE_TITLES[@colour_id]


    @chmoods = Chmood.joins("INNER JOIN users ON chmoods.user_id = users.id INNER JOIN friendships ON users.id = friendships.friend_id").where("friendships.user_id = ?", current_user.id)
    .where('colour_id = ?', @colour_id)
    .order('created_at DESC') #.includes(:user)

    ActiveRecord::Associations::Preloader.new(@chmoods, :user).run # because we will call .user for each chmood



  end
end
