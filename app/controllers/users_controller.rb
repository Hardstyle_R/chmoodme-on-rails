class UsersController < ApplicationController
  def show
    @user  = User.find_by(:id => params[:user_id])

    if (@user.nil?)
      return redirect_to user_show_path(:user_id => current_user.id)
    end
    @new_chmood = Chmood.new(:user_id => @user.id, :colour_id => 1, :type_id => 1)

    @following_count = @user.friends.count
    @followers_count = @user.inverse_friends.count


  end

  def list

  end


  def toggle_follow
    @toggle_user = User.find_by(:id => params[:user_id])

    if current_user.is_friend? @toggle_user
      current_user.remove_friend @toggle_user
      flash[:notice] = "You're no longer following @#{@toggle_user.email}"
    else
      current_user.add_friend @toggle_user
      flash[:notice] = "You're now following @#{@toggle_user.email}"
    end

    return redirect_to user_show_path(:user_id => @toggle_user)
  end

end
