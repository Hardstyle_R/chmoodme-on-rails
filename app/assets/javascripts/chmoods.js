// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.



/**
 Bootstrap Alerts -
 Function Name - showalert()
 Inputs - message,alerttype
 Example - showalert("Invalid Login","alert-error")
 Types of alerts -- "alert-error","alert-success","alert-info"
 Required - You only need to add a alert_placeholder div in your html page wherever you want to display these alerts "<div id="alert_placeholder"></div>"
 Written On - 14-Jun-2013
 **/

function showalert(message, alerttype, hide_after) {

    if (hide_after == undefined) {
        hide_after = 1500
    }

    $('#container').prepend('<div class="alert-messages-fixed"><div id="alertdiv" class="alert ' +  alerttype + '"><button type="button" class="close" data-dismiss="alert">×</button><span>'+message+'</span></div></div>');

    setTimeout(function() { // this will automatically close the alert and remove this if the users doesnt close it in 5 secs


        $("#alertdiv").fadeOut(500, function() { $(this).remove()} );

    }, hide_after);
}


$(document).ready(function () {


//    var $handler = $('#tiles li');
//
//    var $tiles = $('#tiles'),
//        $handler = $('li', $tiles),
//        $main = $('#main'),
//        $window = $(window),
//        $document = $(document),
//        options = {
//            // Prepare layout options.
//            autoResize: true, // This will auto-update the layout when the browser window is resized.
//            container: $('#main'), // Optional, used for some extra CSS styling
//            offset: 24, // Optional, the distance between grid items
//            outerOffset: 10, // Optional, the distance to the containers border
//            itemWidth: "30%" // Optional, the width of a grid item
//            ,flexibleWidth: true
//            ,align: 'left'
//
//        };
//
//    /**
//     * Reinitializes the wookmark handler after all images have loaded
//     */
//    function applyLayout() {
//
//        console.log('here');
//
//        $tiles.imagesLoaded(function() {
//            // Destroy the old handler
//            if ($handler.wookmarkInstance) {
//                $handler.wookmarkInstance.clear();
//            }
//
//            // Create a new layout handler.
//            $handler = $('li', $tiles);
//            $handler.wookmark(options);
//
//
//        });
//
//    }
//
//    // Call the layout function for the first time
//    //applyLayout();
//    $handler.wookmark(options);




//    var options = {
//            // Prepare layout options.
//            autoResize: true, // This will auto-update the layout when the browser window is resized.
//            container: $('#main'), // Optional, used for some extra CSS styling
//            offset: 24, // Optional, the distance between grid items
//            outerOffset: 10, // Optional, the distance to the containers border
//            itemWidth: "30%" // Optional, the width of a grid item
//            ,flexibleWidth: true
//            ,align: 'left'
//
//        };
//
//
//    $('#tiles li').wookmark(options);



//    var $container = $('#tiles');
//// initialize Masonry after all images have loaded
//    $container.imagesLoaded( function() {
//        $container.masonry({
//            "isInitLayout": true,
//            "columnWidth": 80,
//            "itemSelector": 'li',
//            "gutter": 24
//        });
//    });





    ////////////
//    alert('aaa');
//
//    var container = document.querySelector('#tiles');
//    var msnry = new Masonry( container, {
//        // options
//        columnWidth: 110,
//        itemSelector: 'li',
//        animate: true,
//        animationOptions: {
//            duration: 700,
//            queue: true
//        }
//    });
//
//    msnry.on( 'layoutComplete', function () {
//        console.log('laout done');
//    } );

//    $('*[name=comment_body]').each(function() {
//
//        var chmood_id = $(this).attr('id').match(/\d+/)[0];
//        var comment_body = $(this).val();
//
//        console.log(chmood_id);
//        console.log(comment_body);
//
//
//
//    });


//    $('*[name="commit-new-submit"]').hide();
//    $('*[name=comment_body]').on('focus', function() {
//        $(this).parent().find('*[name="commit-new-submit"]').show();
//    });
//
//    $('*[name=comment_body]').on('blur', function() {
//        $(this).parent().find('*[name="commit-new-submit"]').hide();
//    });


//    jQuery('.comment-new-form').each(function(e) {
//
//        $(this).on('keypress', function(e) {
//            if (e.keyCode == 13 && !e.shiftKey) {
//                e.preventDefault();
//                jQuery.ajax({
//                    url: "/my/URL",
//                    data: jQuery(this).form.serialize(),
//                    success: function(){},
//                    dataType: "json"
//                });
//            }
//        });
//
//
//    });


    $('*[name=comment_body]').on('keypress', function(e) {
        if (e.keyCode == 13 && !e.shiftKey) {
            e.preventDefault();
            var chmood_id = $(this).attr('id').match(/\d+/)[0];
            $.ajax({
                method: 'post',
                url: "/chmoods/" + chmood_id + "/comments",
                data: {
                    comment:  $(this).val(),
                    user_id: 777
                },
                success: function(data, textStatus, xhr){ showalert('Well done! You can praise yourself!', 'alert-success', 2000) },
                error: function() { showalert('Ooops, smth gone wrong((', 'alert-danger', 3000)},
                dataType: "json"
            });
        }
    });





});

