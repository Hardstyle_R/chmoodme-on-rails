ActiveAdmin.register Chmood do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  index do
    selectable_column
    id_column
    column :user
    column :type_id
    column :colour_id
    column :content
    column :desc
    column :image_file_name
    column :image_updated_at
    column :updated_at
    actions
  end
  
end
