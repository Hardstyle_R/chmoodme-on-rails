class User < ActiveRecord::Base


  include Gravtastic

  is_gravtastic! # :size => 64

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  has_many :chmoods, :dependent => :destroy # delete chmoods if user has been deleted

  has_many :friendships
  has_many :friends, :through => :friendships

  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user



  def add_friend(friend)
    friendship = friendships.build(:friend_id => friend.id)
    if !friendship.save
      logger.debug "User '#{friend.email}' already exists in the user's friendship list."
    end
  end


  def remove_friend(friend)
    # friendship = friendships.find(:friend_id => friend.id)
    friendship = Friendship.where(:user_id => self.id, :friend_id => friend.id).first;
    if friendship
      friendship.destroy
    end

  end

  def is_friend?(friend)
    friends.include? friend
  end

end
