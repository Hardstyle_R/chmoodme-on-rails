class Chmood < ActiveRecord::Base

  belongs_to :user

  validates_presence_of :user_id, :colour_id, :type_id, :message => "can't be blank"

  validates :content, length: { maximum: 256 }
  validates :content, length: { maximum: 256 }

  default_scope order('created_at DESC')


  has_attached_file :image, styles: {
      thumb: '', #128x128>',
      square: '', #200x200#',
      medium: '' #'300x300>'
  },
  :convert_options => {
      :thumb => Proc.new { |instance| instance.thumnail_dimension(64) },
      :medium => Proc.new { |instance| instance.thumnail_dimension(210) }
  }

  # http://stackoverflow.com/questions/15423330/how-to-make-paperclip-crop-and-not-scale-an-attached-image
  def thumnail_dimension(size=100)
    dimensions = Paperclip::Geometry.from_file(image.queued_for_write[:original].path)
    min = dimensions.width > dimensions.height ? dimensions.height : dimensions.width
    "-gravity Center -crop #{min}x#{min}+0+0 +repage -resize #{size}x#{size}^"
  end

  # Validate content type
  validates_attachment_content_type :image, :content_type => /\Aimage/
  # Validate filename
  validates_attachment_file_name :image, :matches => [/png\Z/, /jpe?g\Z/]
  # Explicitly do not validate
  # do_not_validate_attachment_file_type :image
  #validates_attachment_presence :image

  acts_as_commentable

  validate :content_by_type

  def content_by_type
    errors.add(:content, "text required") if (self.content.empty? && self.type_id == 1)
  end




end
