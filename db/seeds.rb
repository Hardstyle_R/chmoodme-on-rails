# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require 'ffaker'
require 'populator'


User.destroy_all

100.times do

  password = 'hardstyler'
  user = User.create( :email => Faker::Internet.email,
                      :password => password,
                      :password_confirmation => password)
  user.save!
end

User.all.each do |user|

  # adding a couple of posts...
  Chmood.populate(5..15) do |chmood|
    chmood.user_id = user.id
    chmood.colour_id = (1..5).to_a.shuffle.last
    chmood.type_id = 1 # means text entry
    chmood.content = Faker::Lorem.sentence
    chmood.desc = Faker::Lorem.sentence
    chmood.tags = Faker::Lorem.words(5).join(',')
  end


  # adding a couple of friends...
  rand(5..10).times do
    user.add_friend User.all[rand(User.count)]
  end
end