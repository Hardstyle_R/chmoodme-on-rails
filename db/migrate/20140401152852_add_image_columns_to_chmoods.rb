class AddImageColumnsToChmoods < ActiveRecord::Migration
  def change
    add_attachment :chmoods, :image
  end
end
