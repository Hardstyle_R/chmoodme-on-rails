class CreateChmoods < ActiveRecord::Migration
  def change
    create_table :chmoods do |t|
      t.integer :user_id, :null => false
      t.integer :colour_id, :null => false
      t.integer :type_id, :null => false
      t.string :content
      t.string :desc
      t.string :tags

      t.timestamps
    end
  end
end
